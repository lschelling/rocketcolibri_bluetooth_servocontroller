# README #

The Android RocektColibri application is available at:
https://bitbucket.org/lschelling/rocketcolibri_android

### RocketColibri Bluetooth ServoController ###

The RocketColibri Bluetooth ServoController is the Bluetooth receiver of the RocketColibri project. It allows to use any Arduino device as a RC receiver. Existing RC components, (ESC, servos) can be connected to the Arduino board. The RocketColibri Android application allows to control these components from your Android phone.

The Bluetooth ServoController is available in a PPM and PWM version. The PPM (pulse pause modulation) version has all servo signal on one pin. A flight controller can be connected over just one signal wire. The PWM version provides 8 PWM singals. Each signal can be connected to a servo or ESC.

### hardware setup Bluetooth module ###

For the bluetooth communication the Arduino board must be equipped with a serial Bluetooth module. Take care that the Rx pin of the module is connected with the Tx pin and the Rx pin of the module is connected to the Tx pin of the Arduino board.

There exists different versions of Arduino and Bluetooth modules with diffrent requirements according the supply and singal voltage. You have to consult the specs before connecting the devices to be sure not to damage your equippment.

### PPM output ###
The PPM output is on pin 9 of the Aduino board.

### setup bluetooth connection ###

Before connecting the RocketColibri application on the Android phone with the Bluetooth module the Andorid phone must be bound to the Bluetooth module. This can be done in the Bluetooth settings of the phone. Typical password to bind a serial Bluetooth device are 0000 or 1234.
