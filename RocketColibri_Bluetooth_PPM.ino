/* -----------------------------------------------------------------------------
** 
* Project: RocketColibriBlue
* 
* This is the Bluetooth to PPM converter for the RocketColibri bluetooth mode
* 
* It makes use of the functions from the RClib: http://sourceforge.net/p/arduinorclib/
* 
*
* The RocketColibriBlue expectes to receieve the channel date over bluetooth SSP
* in the following format:
* 1=212|2=123|3=234|1=212|2=123|3=234|
* 
* 
* <channel>=<value>
* 
* channel 1..4
* value 1..1000
* The PPM output will be updated after every received separator character '|'.
* 
* -----------------------------------------------------------------------------*/

#include <PPMOut.h>
#include <Timer1.h>

#define CHANNELS 8

// global variable
String inputString = ""; 
boolean stringComplete = false; 

uint16_t g_input[CHANNELS];                   // Input buffer in microseconds
uint8_t  g_work[PPMOUT_WORK_SIZE(CHANNELS)];  // we need to have a work buffer for the PPMOut class

// PPMOut requires two buffers:
//     Input buffer containing input samples in microseconds
//     Work buffer of ((channels + 1) * 2) elements for internal calculations and frame buffering
// This setup removes any limit on the number of channels you want, and makes sure the library doesn't use more
// memory than it really needs, since the client code supplies the buffers.
rc::PPMOut g_PPMOut(CHANNELS, g_input, g_work, CHANNELS);

void setup()
{
  // Initialize timer1, this is required for all features that use Timer1
  // (PPMIn/PPMOut/ServoIn/ServoOut)
  rc::Timer1::init();
  for (uint8_t i = 0;  i < CHANNELS; ++i)
  {
    g_input[i] = map(0, 1, 1000, 1000, 2000);
  }
  // initialize PPMOut with some settings
  g_PPMOut.setPulseLength(448);   // pulse length in microseconds
  g_PPMOut.setPauseLength(10448); // length of pause after last channel in microseconds
    // start PPMOut, use pin 9 (pins 9 and 10 are preferred)
  g_PPMOut.start(9);
  // initialize serial:
  Serial.begin(9600);
  inputString.reserve(200);
}

int getChannelNumber()
{
  return inputString.substring(0,1).toInt()-1;
}

int getChannelValue()
{
  return inputString.substring(2).toInt();
}

/*
 SerialEvent occurs whenever a new separator character '|' comes in the
 hardware serial RX.  
 
 This routine is run between each time loop() runs, so using delay inside
 loop can delay response. 
*/
void serialEvent() 
{
  while (Serial.available()) 
  {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '|' || inChar == '\n') 
    {
      stringComplete = true;
    }
  }
}

/**
 * Main loop
 */
void loop()
{
  serialEvent(); // wait for a serial event (received string)
  if (stringComplete) 
  {
    stringComplete = false;
    int channel = getChannelNumber(); // parse channel number
    if (channel < CHANNELS)
    {
      // fill input buffer, convert raw values to microseconds
      g_input[channel] = map(getChannelValue(), 1, 1000, 1000, 2000); // update PPM input
      g_PPMOut.update(); // and update PPM timer
    }
    else
    {
      Serial.println("ERROR Channel:" + String(getChannelNumber()) + " value:" + String(getChannelValue()));
    }
    inputString = "";
  }
}
